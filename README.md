# RIE


## installation

sur Debian flavored distro
```bash
sudo apt-get install python3 python3-venv git
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

## configuration

Création du dossier mkdocs
```bash
cd rie
mkdocs new .
```

- fichier de configuration => `index.yml`
- page d'accueil => `docs/index.md`

création d'une configuration basique
```bash
touch mkdocs.yml
cat >> mkdocs.yml << EOF
site_name: RIE
theme:
  name: material
EOF
```

## déployer

pour lancer une instance en local
```bash
cd rie
mkdocs serve
```

pour publier le site sur gitlab
```bash
touch gitlab-ci.yml
cat >> gitlab-ci.yml << EOF
image: python:latest
pages:
  stage: deploy
  script:
    - python -m pip install -r requirements.txt
    #building
    - python -m mkdocs build --site-dir public
  artifacts:
    paths:
      - public
  #rules:
  #  - if: '$CI_COMMIT_BRANCH == "prod"'
EOF
```

Il vaut mieux écrire une documentation sur une branche à part, puis merge, de sorte à ce que les docs pas encore terminée ne soit pas publié sur le site.

## Pages

Lien vers le mkdocs [ici](https://rie-ralsei38-20c389dbc122a203b5562f6750ea67db95dec6bf4d8d9800d2.gitlab.io/)

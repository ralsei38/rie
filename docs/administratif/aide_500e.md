# Prime d'aide au 1er équipement. 

## Présentation
**Objectif :** Aider l'alternant à s'équiper avec du matériel qui l'aidera dans le cadre de son alternance.  
**Type :** Aide financière.  
**Montant :** 500€ maximum rendu sous forme de chèque.  
**Conditions :**  
    - Être alternant dans une structure privée (avoir un OPCO).  
    - Faire un achat pendant la période couvrant les deux ans de l'alternance.   
    - Avoir une facture de cet achat.   
    - Avoir une boite postale.     

## Déroulement
1. Effectuer un achat de matériel (peut dépasser 500€).
2. Demander une facture de cet achat.
3. Envoyer la facture à l'adresse e-mail suivante : **gestionnaire1@formasup-ida.com** en précisant votre situation (alternant, école, promotion...)
4. Au bout d'une semaine ou deux, vous receverez un chèque par voie postale qu'il faudra encaisser. 

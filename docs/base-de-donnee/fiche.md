# Dépendance fonctionnelles & Forme Normale

## Dépendance fonctionnelle
Une dépendance fonctionnelle est une contrainte entre deux ensembles d'attributs dans une relation (table) d'une base de données.  Dans une relation R, on dit qu'il y a dépendance fonctionnelle entre un ensemble d'attributs A et un ensemble d'attributs B, ou que l'ensemble A d'attributs détermine l'ensemble B d'attributs (et on écrit A→B) si quand deux n-uplets coïncident sur leurs attributs A, alors ils coïncident aussi sur leurs attributs B.

Le fait de ne pas tenir compte d'une dépendance fonctionnelle peut générer :

- Des pertes d’information
- Des incohérences de données
- Des redondances de données

### Exemple
Des fournisseur avec la relation R( __Projet, Produit, Fournisseur,__ AdresseFournisseur, NombreDePiece)
Le fournisseur a une adresse et approvisionne N pièces d'un produit pour un projet

| Projet  | Produit | Fournisseur | Adresse  | NBpiece |
|:-------:|:-------:|:-----------:|:--------:|:-------:|
| projet1 | écrou   | paul        | Grenoble | 10      |
| projet1 | boulon  | paul        | Grenoble | 10      |
| projet2 |  vis    | Pierre      | Paris    | 50      |
| projet3 | rondelle| jean        | Grenoble | 100     |
| projet3 | écrou   | pierre      | Paris    | 30      |
| projet2 | boulon  | paul        | Grenoble | 20      |

**Dependance fonctionnelle :**

DF elementaire :
fournisseur, produit → adresse fournisseur non élémentaire car fournisseur → adresse fournisseur

DF redondante:
si Projet -> Nombre de pièce, projet -> pièce et pièce -> Nombre de pièce. Alors pièce -> Nombre de pièce est redondante

**Anomalie :**

Anomalie de mise a jours :
Si on change l'adresse de paul, il faut changer tout les n-uplay ou paul apparaît

Anomalie d'insertion :
On ne peut introduire un fournisseur que s’il approvisionne une pièce pour un projet

Anomalie de suppression :
La suppression des entités où se trouve le projet pr3 nous fait perdre de l’information concernant le fournisseur Jean et en particulier son adresse

## Normalisation
La normalisation permet de minimiser la redondance de l’information et assurer la cohérence de la base
NF1 :

- tous ses attributs ont une valeur unique

NF2 :

- NF1
- tous les attributs non-clés sont directement dépendants de l'ensemble des clés

NF3 :

- NF2
- Il n'y a pas d'élément non clef dépendant d'élément non clef

## SQL

**SELECT :**

`SELECT *` Permet d’afficher tous les champs disponibles de toutes les tables « sélectionnées »

```sql
SELECT champ1, champ2, ...,champN
SELECT champ1 AS Alias1, champ2 AS Alias2, ...,champN AS AliasN
```
Permet d’afficher 1 ou plusieurs champs parmi les champs disponibles

**FROM :**
```sql
FROM UneTable1, UneTable2, ..., UneTableN
FROM UneTable1 AS Alias1, UneTable2 AS Alias2, ..., UneTableN AS AliasN
```
Permet de préciser les tables à utiliser.

**WHERE :**

``WHERE Conditions``
Permet de préciser les lignes à conserver ou retirées de la « sélection » (de la projection). Les conditions sont des expressions booléennes (voir fiche sur l’algorithmique) portant sur les champs (les colonnes) des lignes. On parle de restriction

**Fonction :**
COUNT(*), COUNT(unChamp), COUNT(DISTINCT unCham), AVG(unChamp), SUM(unChamp), MIN(unChamp), MAX(unChamp)

**ORDER BY :**

`ORDER BY champ1, champ2 ASC, champ3 DESC,...```
Permet de trier les lignes (résultat de la requête) en fonction des champs. Le ASC (ascendant) permet de trier par ordre croissant, DESC (descendant) par ordre décroissant. Ne rien mettre équivaut à trier par ordre croissant (ASC)

**LIMIT OFFSET :**

`LIMIT nombreLignes OFFSET premiereLigne`
Permet de ne retourner qu’une partie des lignes, à savoir permet de retourner « nombreLignes » lignes à partir de la ligne « premiereLigne ». Attention ! La première ligne est la ligne 0

```sql
SELECT DISTINCT Etudiant.Prenom, Etudiant.Nom -- Remarquez la nécessité d’ajouter DISTINCT
FROM Notes AS N1, Etudiant                    -- afin d’éviter un doublon (duplicata, i.e. 2x le même
WHERE N1.NumEtudiant = Etudiant.NumEtudiant étudiant*)
AND N1.Year = 2016
AND N1.Note = (
  SELECT Max(N2.Note)
  FROM Notes As N2                            -- L’alias lève ici tout ambiguïté (dans le doute...)
  WHERE N2.Year = 2016
)
```

```sql
select titre
from salle
where titre in(
	select titre
	from film
	where metteurscene = 'Sofia Coppola')
group by titre
having count(distinct nomcine) = 2;
```

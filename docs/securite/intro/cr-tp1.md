# TP1 : Retour à l'essenciel

groupe : Arthur Gaillard, Elian Loraux et Lillian Steimer

## Sujet

[Sujet TP1 PDF](tp1-sujet.pdf)


## Exercice 1
Donnez l’adresse IP de la machine hôte ainsi que de la machine virtuelle sur le réseau Host-only
`ip a` ou  `ip addr`

Pour changer une adresse mac :
```bash
ip link set dev wlp0s20f3 down

ip link set dev wlp0s20f3 address 84:A9:3E:60:5B:35
hostname Eric
echo "Adresse mac wire-less changé pour un HP | Hostname : Eric"

ip link set dev wlp0s20f3 up
```


## Exercice 2
1. Créez deux groupes d’utilisateur.
2. Créez un utilisateur par groupe, plus un troisième qui devra se trouver dans les deux groupes.

```bash
addgroup [options] [--gid ID] groupe

adduser  [options]  [--home  DIR]  [--shell  SHELL] [--no-create-home] [--uid ID] [--firstuid ID] [--lastuid ID] [--ingroup GROUP | --gid ID] [--disabled-password] [--disabled-login] [--gecos GECOS]
       [--add_extra_groups] [--encrypt-home] user
```

```bash
addgroup group1
addgroup group2

adduser -ingroup group1 user1
adduser --ingroup group2 user2
adduser --ingroup group1 --ingroup group2 user3
```

## Exercice 3
1. Mettez à jour le cache de dépôt APT et installez apache2.
2. Retirez le site par défaut d’apache (nom du site: default) et configurez Apache2 avec deux sites web (une
simple page html sera suffisant par site).
  - Un site accessible publiquement
  - Un site accessible uniquement par certains utilisateurs (utilisez .htpasswd).

`apt update && apt install apache2`

Le dossier exposé de apache se trouve dans */var/www/html*

__htacces__
```
AuthType Basic
AuthName "Accès protégé"
AuthUserFile /var/www/html/private/.htpasswd
require valid-user
```
__htpasswd__
```
admin:admin
```

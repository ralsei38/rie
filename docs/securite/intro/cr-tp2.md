# TP2 : Outils

groupe : Arthur Gaillard, Elian Loraux et Lillian Steimer

## Sujet

[Sujet TP2 PDF](tp2-sujet.pdf)


## SSH

### Authentification du serveur

>Q1. Installez openssh-server sur votre machine virtuelle.

Pour installer `openssh-server`, nous devons simplement mettre à jour nos systèmes avec les commandes suivantes :

```bash
sudo apt update
sudo apt upgrade
```

Ensuite, vous pouvez installer `openssh-server` :

```bash
sudo apt install openssh-server
```

Une fois que c'est fait, vous pouvez vérifier si votre serveur SSH fonctionne avec la commande `systemctl status sshd`.

>Q2. Créez vos propres clés "hôte" dans /etc/ssh :

Supprimez les anciennes clés dans /etc/ssh/ (les fichiers ssh_host_*_key)
Créez de nouvelles paires de clés RSA et DSA avec :
`ssh-keygen -t rsa -b 4096 -f /etc/ssh/safe_host_rsa_key` (faites de même pour DSA)
Mettez à jour /etc/ssh/sshd_config pour utiliser ces nouvelles clés (directive Hostkey)
Connectez votre client au serveur : ssh utilisateur@IP_SERVEUR -p PORT (si vous utilisez une configuration NAT, vous aurez besoin d'un port différent, en fonction de votre mappage)

Pour supprimer toutes les anciennes clés dans /etc/ssh, j'utilise la commande : `rm -f /etc/ssh/ssh_host__key`
Ensuite, je crée la nouvelle clé en utilisant l'outil `ssh-keygen` avec la commande fournie.
Dans le fichier /etc/ssh/sshd_config, j'ajoute ou décommente la directive `HostKey` et spécifie le fichier de clé fraîchement généré.

```yml
HostKey /etc/ssh/safe_host_rsa_key
HostKey /etc/ssh/safe_host_dsa_key
```

Après cela, un rechargement ou un redémarrage est nécessaire pour que les modifications prennent effet.

Maintenant, je peux me connecter à mon `openssh-server` en utilisant la commande suivante du côté client en utilisant `ssh utilisateur@IP_SERVEUR -p PORT`. Ici, l'adresse IP de mon serveur est `192.168.122.1` et le port est `2222`, donc la commande correcte est la suivante :

```bash
ssh root@192.168.122.1 -p 2222
```
### Authentification de l'utilisateur

>Q3. Il existe différentes façons de gérer l'authentification des utilisateurs :

Utilisation du couple nom d'utilisateur / mot de passe
Utilisation d'une clé publique / privée (voir : https://en.wikipedia.org/wiki/Public-key_cryptography) Il est préférable d'utiliser une phrase de passe complexe à ce stade)
Cela créera 2 fichiers dans ~/.ssh/ : id_rsa, id_rsa.pub (ou id_dsa) . Le fichier .pub est la clé publique, l'autre est la clé secrète (et par secrète, nous voulons dire qu'il ne faut jamais la donner à personne). La clé publique peut être ajoutée à n'importe quel fichier ~/.ssh/authorized_keys sur le serveur SSH auquel vous souhaitez vous connecter.

### Pratique

>Q4. Créez sur votre hôte un couple de clés publique/privée, et ajoutez la clé publique au serveur. Trouvez comment configurer votre serveur SSH pour interdire la connexion par mot de passe (dans /etc/ssh/sshd_config).

1. D'abord, j'utilise la commande suivante pour créer un nouveau couple de clés rsa :

```
ssh-keygen -t rsa -b 4096
```

2. Ensuite, j'utilise la commande `ssh-copy-id -i /home/lilian/.ssh/id_rsa -p 2222 root@192.168.122.1` pour copier ma clé publique personnelle dans le fichier authorized_keys sur le serveur. Cela me permettra de me connecter à distance au serveur en utilisant mon couple de clés.

3. Maintenant, je peux me connecter au serveur sans utiliser de mot de passe en utilisant la commande suivante :

```bash
ssh root@192.168.122.1 -p 2222
```

## Fail2ban

### Pratique

#### Les filtres par défaut

Par défaut fail2ban est livré avec de nombreuses régles. Parmis ces régles celles qui peuvent nous intéreser pour notre serveur sont celles concernant les accès à notre serveur SSH et celles concernant notre serveur HTTP (Apache2).

Ces régles surveillent les journaux pour détecter les échecs répétés de connexion ou d'autres comportements suspects, et elles bloquent temporairement les adresses IP sources responsables de ces comportements en utilisant des règles `iptables`.

Cela aide à renforcer la sécurité de votre serveur en limitant l'accès aux adresses IP qui semblent malveillantes ou qui tentent de compromettre la sécurité du serveur.

#### Les filtres personnalisé

Création du filtre personnalisé pour les requêtes DFind w00tw00t :

Tout d'abord, créez un filtre personnalisé en créant un fichier `apache-w00tw00t.conf` dans le répertoire `/etc/fail2ban/filter.d/` (assurez-vous que le répertoire `filter.d` existe). Vous pouvez utiliser un éditeur de texte pour créer ce fichier. Voici le contenu du fichier `apache-w00tw00t.conf` :

```plaintext
[Definition]
failregex = ^<HOST> -.*"GET \/w00tw00t\.at\.ISC\.SANS\.DFind\:\).*".*
ignoreregex =
```

- `failregex`` : C'est le modèle de correspondance des journaux. Il identifie les lignes de journal qui correspondent à la requête DFind w00tw00t.

Configuration du jail (prison) :

Ensuite, ajoutez la configuration du jail dans le fichier /etc/fail2ban/jail.local ou créez-le s'il n'existe pas. Voici la configuration pour le jail apache-w00tw00t :

```plaintext
[apache-w00tw00t]
enabled = true
filter = apache-w00tw00t
action = iptables[name=Apache-w00tw00t,port=80,protocol=tcp]
banaction = iptables-allports
logpath = /var/log/apache2/access*.log
maxretry = 1
banTime = 86400
```

- `enabled` = true : Cela active le jail pour surveiller les journaux Apache à la recherche de requêtes DFind w00tw00t.

- `filter` = apache-w00tw00t : Spécifiez le nom du filtre personnalisé que vous avez créé.

- `action` : Définit l'action à prendre en cas de correspondance avec le filtre. Dans ce cas, il utilise iptables pour bloquer l'adresse IP source.

- `banaction` = iptables-allports : Cette action bloque l'adresse IP sur tous les ports.

- `logpath` : Indiquez le chemin des fichiers de journaux Apache que Fail2ban devrait surveiller. Dans cet exemple, il surveille les fichiers access.log dans le répertoire /var/log/apache2/.

- `maxretry` = 1 : Cela signifie que seulement une correspondance est nécessaire pour déclencher l'action de blocage.

- `banTime` = 86400 : La durée pendant laquelle l'adresse IP sera bloquée (en secondes).

Une fois la régle en place, nous pouvons vérifier le bon fonctionnement de celle-ci, en utilisant la commande suivante :

```shell
fail2ban-regex /var/log/apache2/access.log /etc/fail2ban/filter.d/apache-w00tw00t.conf
```

Cette commande simule l'analyse des journaux en utilisant le filtre que vous avez créé et indique si la règle est correctement configurée.

Il faut ensuite redémarrer ``Fail2ban`` après avoir apporté ces modifications pour qu'elles prennent effet.

## Wireshark

Wireshark est un outil de capture de paquet. Il permet d'avoir une vision des paquets qu'on recoit e>
qu'on envoit depuis tout l'ordinateur ou depuis une interface en particulier. Il permet notamment
de décortiquer les paquets et d'avoir l'information les contenants et permet donc de faire des attaq>
selon la protection utilisé.

- Capture de traffic SSH (voir la capture ssh.pcapng)
Dans la capture, on peut voir une connection ssh en train de se créer, notamment avec le drapeau SYN.

![ssh](ssh.png)

- Capture d'un ping vers google (voir la capture ping.pcapng)
Dans la capture, on voit les paquets ICMP echo request et echo reply.
On voit aussi des paquets DNS query pour l'adresse IP de www.google.com

![ping](ping.png)


## Nmap

Nmap est un outil qui permet de scanner des machines via une adresse IP ou une plage d'adresse IP.
Il permet notamment de connaitres les ports ouverts sur les machines et d'avoir plus d'informations
sur les services et leur version.

$ sudo nmap -sT 192.168.100.1

Starting Nmap 7.80 ( https://nmap.org ) at 2023-11-07 15:43 CET
Nmap scan report for LT-Arthur (192.168.100.1)
Host is up (0.000089s latency).
Not shown: 997 closed ports
PORT     STATE SERVICE
53/tcp   open  domain
2222/tcp open  EtherNetIP-1
8080/tcp open  http-proxy

L'option -sV permet d'avoir des informations supplémentaire concernant un service ouvert.

## john the ripper

```
root@quagga:~# john -wordlist:/usr/share/dict/french /etc/shadow
Loaded 3 password hashes with 3 different salts (crypt, generic crypt(3) [?/64])
Press 'q' or Ctrl-C to abort, almost any other key for status
abandon          (toto)
```

## Conclusion

Ce TP n'a été que des révision de l'année derrnière en licence pro administration système, réseau et cyber sécurité. De plus, Lilian et Elian administre un serveur personnel et à mis en place fail2ban. De plus, nmap, john the ripper et Wireshark sont la base des outils utilisé en Capture The Flag (concours de cybersécurité).

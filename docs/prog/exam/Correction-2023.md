# Exam 2023

!!! note
    Examen de programmation de 2023

!!! warning
    Les sujet change légérement d'une années à l'autre.

## Sujet

[Sujet 2023 PDF](Sujet-2023.pdf)

## Pseudo Correction

### Exercice 1
1) Qu'est-ce qu'une fonction ?

Une fonction est une séquence d'instructions regroupées sous un nom spécifique et conçues pour effectuer une tâche particulière. Les fonctions sont utilisées pour organiser le code en le subdivisant en morceaux plus petits et réutilisables.

2) Qu'est-ce que la signature d'une fonction

La signature d'une fonction se réfère à la manière dont une fonction est déclarée, notamment son nom, ses paramètres et son type de retour. La signature définit la structure de base de la fonction et permet de spécifier comment la fonction peut être appelée.

3) Donner un exemple d'une définition d'une fonction et son appel

```c
int add(int a, int b){
  return a + b;
}

int main(){
  int sum = add(5, 10);

  return 1;
}

```

4) Expliquer les deux modes de passage de paramètre : par valeur et par références

Un passage par valeur ou par copie, comme son num l'indique, copie la valeur d'une variable à l'appel de la fonction. Donc, la variable placé en paramètre à l'appel de la fonction et la variable dans le corp de la fonction ne sont pas les mêmes, l'un est une pale copie de l'autre.

Un passage par référence permet de donner l'adresse d'une variable en mémoire. Par conséquent a variable placé en paramètre à l'appel de la fonction et la variable dans le corp de la fonction pointe vers la même choses.  

5) Si une fonction modifie localement un de ses paramètre formels, tableau, cette modification est-elle répercutée sur l'appelant ?

Pour un tableau, si une fonction modifie localement un tableau place en paramètre, cette modification sera répercutée sur l'appelant l'élement place en paramètre est le pointeur vers le premier element du tableau

### Exercice 2
1) Que représente le nom d'un tableau dans le langage C ?
Le nom d'un tableau représente un pointeur vers la première valeur de ce tableau.

2) Ecrire une fonction `init()` qui initialise à 0 tous les éléments d'un tableau à trois dimention. Ecrire une fonction qui affiche ce même tableau sur la sortie strandard.

```c
#define TABLESIZE 3

void init(int table[TABLESIZE][TABLESIZE][TABLESIZE]){
  for (int i = 0; i < TABLESIZE; i++) {
    for (int j = 0; j < TABLESIZE; j++) {
      for (int k = 0; k < TABLESIZE; k++) {
        table[i][j][k] = 0;
      }
    }
  }
}

void affiche(int table[TABLESIZE][TABLESIZE][TABLESIZE]){
  for (int i = 0; i < TABLESIZE; i++) {
    for (int j = 0; j < TABLESIZE; j++) {
      for (int k = 0; k < TABLESIZE; k++) {
        printf("%i",table[i][j][k]);
      }
      printf("\n");
    }
    printf("\n");
  }
}
```

### Exercice 3
Initialiser le tableau tab de la fonction main avec 9 entier satisfaisant au problème. Ensuite, comprendre le programme qui suit en donnant le résultat final, accompagné d'un schéma explicatif.

```c
int tab[] = {1, 3, 2, 2, 1, 3, 2, 3, 1};
```

La fonction `FX()` trie les numéro 1, 2 et 3 dans l'ordre `111222333`. Au vu des nom de variable (b, w, r) cela pourrais faire penser au drapeau français avec "Blue", "White" et "Red", bleu, blanc et rouge.

### Exercice 4

1) Ecrire une fonction qui affiche les éléments du tableau (vecteur) ci-dessous
2) Ecrire une fonction qui donne la taille en nombre de caractère de chacune des chaines de caractère du tableau ci-dessous.
3) Quel est le nombre total de caractère du tableau ci-dessous
4) Ecrire une fonction qui donne le nombre des mots du tableau. Un mot est sépare d'un autre par le caractère espace ' '.
5) Compléter le prohramme suivant pour tester les fonctions précédentes.

**Programme c**
```c
#include<unistd.h>
#include <stdio.h>

int sizeofstr(char* buffer){
  int size = 0;
  while (buffer[size] != 0){
    size++;
  }
  return size;
}

void print(char* buffer){
  int size = sizeofstr(buffer);
  write(1,buffer,size);
}

void afficheVect(char * vecteur[], int size){
  for (int i = 0; i < 8; i++) {
    print(vecteur[i]);
    print("\n");
  }
}

int countAllChar(char * vecteur[], int size){
  int sum = 0;
  for (int i = 0; i < size; i++) {
    sum += sizeofstr(vecteur[i]);
  }
  return sum;
}

int countWord(char* buffer){
  int word = 1;
  int size = sizeofstr(buffer);
  for (int i = 0; i < size; i++) {
    if(buffer[i] == ' '){
      word++;
    }
  }
  return word;
}

int compareStr(char* bufferOne, char* bufferTow){
  int sizeOne = sizeofstr(bufferOne);
  int sizeTow = sizeofstr(bufferTow);
  int egale = 1;
  if (sizeOne != sizeTow){
    egale = 0;
  }
  for (int i = 0; i < sizeOne; i++) {
    if(bufferOne[i] != bufferTow[i]){
      egale = 0;
    }
  }
  return egale;
}

int main()
{
  print("Hello world!\n");

  char * vecteur[] = {
    "Rien n'est impossible en alogrithmique",
    "Il n'y a pas de probleme",
    "qui ne soit sans solution.",
    "on insiste",
    "Il n'y a pas de probleme",
    "qui ne soit sans solution.",
    "Donc s'il n'y a pas de solution",
    "c'est qu'il n'y a pas de probleme"
  };

  afficheVect(vecteur,8);


  print("\n===============================\n");
  for (int i = 0; i < 8; i++) {
    print(vecteur[i]);
    printf("\ttaille de la chaine : %i", sizeofstr(vecteur[i]));
    printf("\tnombre de mot : %i\n", countWord(vecteur[i]));
  }

  printf("Comparaison chaine 2 et 5: %i\n", compareStr(vecteur[1], vecteur[4]));
  printf("Comparaison chaine 1 et 2: %i\n", compareStr(vecteur[0], vecteur[2]));

  return 0;
}
```

### Exercice 5

**Programme Java**
```java
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        String[] vecteur = {
                "Rien n'est impossible en alogrithmique",
                "Il n'y a pas de probleme",
                "qui ne soit sans solution.",
                "on insiste",
                "Il n'y a pas de probleme",
                "qui ne soit sans solution.",
                "Donc s'il n'y a pas de solution",
                "c'est qu'il n'y a pas de probleme"
        };

        System.out.println("Affichage du vecteur");
        afficheVect(vecteur);
        System.out.println("Affichage du nombre de caractères");
        nbText(vecteur);
        System.out.println("Nombre de caractères globale : " + nbCharAll(vecteur));
        System.out.println("Nombre de mots globale : " + nbWord(vecteur));

        System.out.println("Compare chaine 1 et 2 : " + compare(vecteur[0], vecteur[1]));
        System.out.println("Compare chaine 2 et 5 : " + compare(vecteur[1], vecteur[4]));

    }

    public static void afficheVect(String[] table) {
        for (String elem : table) {
            System.out.println(elem);
        }
    }

    public static void nbText(String[] table) {
        for (String elem : table) {
            System.out.println(elem.length());
        }
    }

    public static int nbCharAll(String[] table) {
        int sum = 0;
        for (String elem : table) {
            sum += elem.length();
        }
        return sum;
    }

    public static int nbWord(String[] table) {
        int sum = 0;
        for (String elem : table) {
            sum += elem.split(" ").length;
        }
        return sum;
    }

    public static boolean compare(String str1, String str2) {
        return str1.equals(str2);
    }
}

```

**Programme Python**
```py
vecteur = [
    "Rien n'est impossible en alogrithmique",
    "Il n'y a pas de probleme",
    "qui ne soit sans solution.",
    "on insiste",
    "Il n'y a pas de probleme",
    "qui ne soit sans solution.",
    "Donc s'il n'y a pas de solution",
    "c'est qu'il n'y a pas de probleme"
  ]

def affiche_vect(table):
    for elem in table:
        print(elem)

def nb_text(table):
    for elem in table:
        print(len(elem))

def nb_charall(table):
    sum = 0
    for elem in table:
        sum +=len(elem)
    return sum

def nb_word(table):
    sum = 0
    for elem in table:
        sum +=len(elem.split(" "))
    return sum

print("affichage du vecteur")
affiche_vect(vecteur)
print("affichage du nombre de caractère")
nb_text(vecteur)
print("Nombre de caractère globale : " + str(nb_charall(vecteur)))
print("Nombre de mot globale : " + str(nb_word(vecteur)))
```

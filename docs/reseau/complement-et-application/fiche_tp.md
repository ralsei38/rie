# Fiche révision partiel TP 2

## élément à connaître

-   OSPF (Open Shortest Path Firs)
-   BGP

## OSPF

### Théorique

OSPF définie les routes les plus courte dans un AS (autonomous system) chaque routeur annonce les routes de son réseau locale en broadcast groupe multicast pour OSPF = 224.0.0.5 et 224.0.0.6 : Sert à communiquer uniquement entre les routeurs Dans chaque réseau local, il ya un routeur designé et un routeur de backup designé définie par celui qui à la plus petite adresse ip une air peut comporter plusieur réseau local

Le routeurs désigné va maintenir la base de donnée topologie de tout l'AS. C'est lui qui va envoyer les mise à jours sur l'adresse multicast.
Il s'assure de la bonne réception de la mise à jours. Pour cela, chaque routeur recevant la mise a jour envoie un acquittement au routeur désigné qui à envoyé la mise à jour. en bref ses roles sont :

- Créer / gére le network LSA (Link state advertisement) (sert à annoncer des routes)
- Centralise les LSU et les diffuse ensuite en multicast
- Syncronise les nouveaux routeurs sur le réseau
- Mainten la base de donnée topologique

**LSA :**

- LSA Type 1 : annonce d'un réseau entre deux routeur (point à
point)
- LSA Type 2 : annonce d'un réseau de transit (pas de point final
(pc))
- LSA Type 3 : annonce d'un réseau sans issue (on sarrete ici)
- LSA Type 4 : résumé des routes vers les autres aire - LSA Type 5 :

**À retenir :**
- 1 Designated router par réseau et non par aire
- 1 backup Designated router par réseau et non par aire

### Pratique

**Mise en place :** sur routeur cisco

``` {.bash}
routeur>            enable # pour passer en root
routeur\#           configure terminal # passe en mode configuration
routeur(config)     hostname [nom] # change le nom de la machine
nom(config)         show interfaces # voir les interface ~= ip a

# --- pour toute les interfaces --- #
nom(config)         interface fastEthernet[noInterface]/[noLigne]  # pour configurer les interface
nom(config-if)      ip addr [ip] [mask (255.255.255.0)] # donne une adresse ip à l'interface
nom(config-if)      no shutdown # permet d'activer l'interface sans quelle s'arrete
nom(config-if)      exit
# --- fin --- #

nom(config)         routeur ospf [id] # mettre le même id pour tout les routeurs    

# --- pour tout les réseau directement connecté au routeur --- #
nom(config-routeur) netword [ip du reseau] [mask inversé (0.0.0.255)] area [id] # l'air doit être la même sur tout les routeurs
# --- fin --- #

nom(config-routeur) end # permet de revenir des config
nom\#               write # sauvegarde la config

nom\#               show ip route # voir les routes du routeurs
nom\#               show ip ospf database # voir la base de donnée OSPF
```

__Show :__
```
show interfaces status

show ip interface
show ip interface brief
show ip interface <INTERFACE_NAME>
show interface

show ip route # current routing table
show ip protocols # current routing protocols in use !!!

show access-lists

show vlan
show vlan brief
show vlan id <VLAN_ID>
show interfaces <INTERFACE_NAME> switchport

show ip rip database
show ip rip interface
show ip rip neighbors

show ip ospf
show ip ospf interface
show ip ospf neighbor
show ip ospf database
show ip ospf area
show ip ospf route
show ip ospf interface <INTERFACE_NAME>
show ip ospf neighbor <neighbor_IP>
```
## BGP

### Théorique

même but d'OSPF OSPF fait les route interne aux AS, GPG fait le lien entre tout les AS.

### Pratique

[https://www.networklab.fr/bgp-configuration-basique/](https://www.networklab.fr/bgp-configuration-basique)

```bash
R1(config)#router bgp 300 # Tout d’abord, entrer dans le mode BGP en spécifiant le numéro d’AS :
R1(config-router)#neighbor 14.0.0.4 remote-as 100 # Puis ajouter une relation de voisinage :
# Il faut maintenant faire de même sur R4 :
R4(config)#router bgp 100
R4(config-router)#neighbor 14.0.0.1 remote-as 300

show ip bgp summary
show ip bgp
show ip bgp neighbors
show ip bgp paths
```

# Révision réseau compléments et application

!!! note
    Sujet de révision type examen 2024

!!! warning
    Les sujet change légérement d'une années à l'autre.

## Sujet

[Sujet revision 2024 PDF](sujet-revision.pdf)

## Exercice 1 : Ponts

**Message**

racine.cout.id

(destination.cout.annonceur)

**State**

__Port designé (d) :__ port par defaut

__Port racine ou root (r) :__ Dès la première mise à jour du message d'un port, le port désigné deviens racine

__Port Bloqué (b) :__ lorsque que pour une même destination, la nouvelle route annoncé est plus grande que l'ancienne.

Exemple :

| Pont |  msg   | state |                  |
|:----:|:------:|:-----:|:----------------:|
|  1   | 3.0.3  |   R   | Nouvelle entrée  |
|  2   | 3.19.4 |   D   |                  |
|  3   | 3.19.4 |   D   |                  |
|  4   | 3.4.5  |   B   | passage à bloqué |


**Etapes :**

__Initialisation:__

| Pont |  msg  | state |
|:----:|:-----:|:-----:|
|  1   | 4.0.4 |   D   |
|  2   | 4.0.4 |   D   |
|  3   | 4.0.4 |   D   |
|  4   | 4.0.4 |   D   |

__Etape 1:__

| Pont |  msg  | state |
|:----:|:-----:|:-----:|
|  1   | 4.0.4 |   D   |
|  2   | 4.0.4 |   D   |
|  3   | 4.0.4 |   D   |
|  4   | 4.0.4 |   D   |

__Etape 2:__

| Pont |  msg   | state |
|:----:|:------:|:-----:|
|  1   | 3.23.4 |   D   |
|  2   | 3.23.4 |   D   |
|  3   | 3.23.4 |   D   |
|  4   | 3.4.5  |   R   |

__Etape 3:__

| Pont |  msg   | state |
|:----:|:------:|:-----:|
|  1   | 3.0.3  |   R   |
|  2   | 3.19.4 |   D   |
|  3   | 3.19.4 |   D   |
|  4   | 3.4.5  |   B   |

__Etape 4:__

| Pont |  msg   | state |
|:----:|:------:|:-----:|
|  1   | 2.38.4 |   D   |
|  2   | 2.38.4 |   D   |
|  3   | 2.38.4 |   D   |
|  4   | 2.19.5 |   R   |

__Etape 5:__

| Pont |  msg  | state |
|:----:|:-----:|:-----:|
|  1   | 2.4.4 |   D   |
|  2   | 2.4.4 |   D   |
|  3   | 2.0.2 |   R   |
|  4   | 2.4.4 |   D   |


__Etape 6:__

| Pont |  msg   | state |
|:----:|:------:|:-----:|
|  1   | 1.57.4 |   D   |
|  2   | 1.57.4 |   D   |
|  3   | 1.57.4 |   D   |
|  4   | 1.38.5 |   R   |


__Etape 7:__

| Pont |  msg   | state |
|:----:|:------:|:-----:|
|  1   | 1.19.3 |   R   |
|  2   | 1.38.4 |   D   |
|  3   | 1.38.4 |   D   |
|  4   | 1.38.4 |   D   |


__Etape 8:__

| Pont |  msg   | state |
|:----:|:------:|:-----:|
|  1   | 1.19.3 |   B   |
|  2   | 1.23.4 |   D   |
|  3   | 1.19.2 |   R   |
|  4   | 1.23.4 |   D   |


__Etape 9:__

| Pont |  msg  | state |
|:----:|:-----:|:-----:|
|  1   | 1.4.4 |   D   |
|  2   | 1.0.1 |   R   |
|  3   | 1.4.4 |   D   |
|  4   | 1.4.4 |   D   |


**Tableau total :**

msg = route annoncé par B4

|                |   msg  | port stable |
|:--------------:|:------:|:-----------:|
|   generated:   |  4.0.4 |   d.d.d.d   |
| received on 4: |  5.0.5 |             |
|   generated:   |  4.0.4 |   d.d.d.d   |
| received on 4: |  3.4.5 |             |
|   generated:   | 3.23.4 |   d.d.d.r   |
| received on 1: |  3.0.3 |             |
|   generated:   | 3.19.4 |   r.d.d.b   |
| received on 4: | 2.19.5 |             |
|   generated:   | 2.38.4 |   d.d.d.r   |
| received on 3: |  2.0.2 |             |
|   generated:   |  2.4.4 |   d.d.r.d   |
| received on 4: | 1.38.5 |             |
|   generated:   | 1.57.4 |   d.d.d.r   |
| received on 1: | 1.19.3 |             |
|   generated:   | 1.38.4 |   r.d.d.d   |
| received on 3: | 1.19.2 |             |
|   generated:   | 1.23.4 |   b.d.r.d   |
| received on 2: |  1.0.1 |             |
|   generated:   |  1.4.4 |   d.r.d.d   |


## Exercice 2 : BGB

MED : privilégié la plus petite valeurs

LOCAL-PREF: privilégie la plus grande valeurs

### Part 1
C1 et C2 vont privilégier le lien à 40 Gb/s car le MED est plus petit et vont ajouter l'attribut local pref aux annonce iBGP vers C3 et C4

Compétez leurs annonces :

C1 à C2, C3 et C4 : 4.4.4/24, AS-PATH=B A, NEXT-HOP=33.33.33.2, LOCAL-PREF=20

C2 à C1, C3 et C4 : 4.4.4/24, AS-PATH=B A, NEXT-HOP=33.33.3.2, LOCAL-PREF=10

### Part 2
B1 et B2 vont privilégié le lien 40 Gb/s car le MED est plus petit et vont transmettre sur l'AS A en eBGP en garant les MED et vont faire les annonces suivante

B1 vers A1 : 2.2.2/24, AS-PATH=B C, NEXT-HOP=44.44.44.2; MED=10

B2 vers A2 : 2.2.2/24, AS-PATH=BBB C, NEXT-HOP=44.44.4.2; MED=20

### Part 3
A2 à B2 : 4.4.4/24, AS-PATH=A, NEXT-HOP=44.44.4.1; MED = 20

A2 à B2 : 3.3.3/24, AS PATH=A, NEXT-HOP=44.44.4.1; MED = 10

A1 à B1 : 4.4.4/24, AS-PATH=A, NEXT-HOP=44.44.44.1; MED = 10

A1 à B1 : 3.3.3/24, AS-PATH=A, NEXT-HOP=44.44.44.1; MED = 20

Les routeurs B vont ignorer les MED car ils ne sont pas en coopération avec l'AS A.

## Exercice 3 : Controle de congestion
### 1 : FIFO
__AB :__

F1 = 1/2

F2 = 1/2

__BC :__

```
F1 = F1/F1 + 2F3
= 1/2 / 1/2 + 1 + 1
= 1/2 / 5/2
= 1/2 * 2/5
= 1/5
```

```
F3 = 1 / 1/2 + 1 + 1
= 1/1 / 5/2
= 1/1 * 2/5
= 2/5
```

__CD :__
```
F1 = F1 / F1 + 3F4
= 1/5 / 1/5 + 1 + 1 + 1
= 1/5 / 16/5
= 1/5 * 5/16
= 5/80
= 1/16
```

```
F3 = 1 / 1/5 + 1 + 1 +1
= 1 / 5/16
= 5/16
```

### 2 : File équitable
__AB :__
```
F1 = 1/2
F2 = 1/2
```
__BC :__
```
F1 = 1/3
F3 = 1/3
```

__CD :__
```
F1 = 1/4
F4 = 1/4
```

Début total du réseau :
AB : 1/2
BC : 2/3
CD : 1

```
Total = 1/1 + 2/3 + 1/2
= 6/6 + 4/6 + 3/6
= 13 / 6
=~ 2.16
```

### 3 Min / Max

|   F1    |      F2       |   F3    |   F3    |   F4    |   F4    |   F4    |
|:-------:|:-------------:|:-------:|:-------:|:-------:|:-------:|:-------:|
| __1/4__ |      1/4      |   1/4   |   1/4   | __1/4__ | __1/4__ | __1/4__ |
|         | 3/4 / 2 = 3/8 | __3/8__ | __3/8__ |         |         |         |
|         |    __3/4__    |         |         |         |         |         |

## 4 : Controle de congestion TCP

### 1 : Reno ou Tahoe
__Tahoe :__ en cas de perte, Tahoe retombe à zero et repart en slow start.

__Reno :__ en cas de perte, reno essaie de se rattraper. Il divise par deux sa dernière taille de fenêtre. Ce mechanisme fast retransmit/recovery  `/!\ à vérifier`

### 2 : Slow Start
__Slow Start :__ augmentation de X2 sur la fenêtre jusqu'à slow start tresh hold (seuil).

```
TCP 1 = 0-6 et 23-25
TCP 2 = 4-6, 7-8 et 15-19
```

### 3 : Congestion Avoidance

__Congestion avoidance :__ augmentation de la taille de fenêtre de manière linéaire jusqu'à une perte.

C'est la ou c'est linéaire
```
TCP 1 = 6-17 et 18-22
TCP 2 = 8-16
```

## 5 : SNMP

```
get-next(colonne.index)
  valeur.indexe
    valeur.valeur;
  )
```
1 :

get-next (ipAdEntAddr, ipAdEntReasmMaxSize)
on prend la première valeur des colonnes. Donc :

```
get-next(
  ipAdEntAddr.192.56.188.33
    ipAddr.192.56.188.33;
  ipAdEntReasmMaxSize.192.56.188.33
    int.1536;
  )
```

2 :

get-next (ipAdEntAddr.192.56.188.33, ipAdEntReasmMaxSize.192.56.188.33)

```
get-next(
  ipAdEntAddr.192.56.228.33
    ipAddr.192.56.228.33;
  ipAdEntReasmMaxSize.192.56.228.33
    int.1500;
  )
```

## MPLS

[Exercice MPLS 1](exo-mpls.pdf)

### Exercice 1

|    SRC     |    DST     | OUT  |
|:----------:|:----------:|:----:|
|     *      | 192.88/16  | b/15 |
|     *      | 128.178/15 | b/11 |
| 195.221/16 | 129.88/16  | c/17 |

__LSR1 :__

| IN   | OUT  |
|:----:|:----:|
| d/15 | d/15 |
| d/15 | d/15 |
| d/15 | d/15 |

__LSR2 :__

| IN   | OUT   |
|:----:|:-----:|
| a/11 | b/POP |
| a/12 | b/POP |

__LSR4 :__

| IN   | OUT  |
|:----:|:----:|
| a/17 | b/18 |

__LSR2 :__

| IN   |  OUT  |
|:----:|:-----:|
| c/17 | b/POP |
| a/16 | b/POP |


### Exercice 2

Pour VPLS, c'est exactement la même choses que MPLS mais à la place d'une sortie "POP" on met des VLAN (pop/VLAN1, pop/VLAN2, ...)

## Congestion controle : exercice bonus

[Exercice congestion controle](congestion-controle.pdf)

### Exo 1

#### 1 : FIFO

```
AB = F1 + F2
BC = F1 + 2F3
CD = F1 + 2F3 + 4F4
```

__AB :__
```
F1 = 1/2
F2 = 1/2
```

__BC :__
```
F1 = 1/2 / 1/2 + 1 + 1
= 1/2 / 1/2 + 2/2 + 2/2
= 1/2 / 5/2
= 1/2 * 2/5
= 2/10
= 1/5
```

```
F3 = 1 / 1/2 + 1 + 1
= 1 / 5/2
= 1/1 * 2/5
= 2/5
```

__DC :__
```
F1 = 1/5 / 1/5 + 2/5 + 2/5 + 1 + 1 + 1 + 1
= 1/5 / 5/5 + 4
= 1/5 / 5
= 1/5 * 1/5
= 1/25
```

```
F3 = 2/5 / 5
= 2 / 25
```

```
F4 = 1 / 5
= 5 / 25
```

#### 2 : Fair Queueing

__AB :__
```
F1 = F2 = 1/2
```

__BC :__
```
F1 = F3 = 1/3
```

__CD :__
```
F1 = F3 = F4 = 1/7
```

### Exo 2

débit source / débit total * débit du réseau

#### 1 : FIFO

__R1 - R2 :__
```
S1 = (100 / 100+1000) * 1000
= 100/1100 * 1000
= 1/11 * 1000
= 1000/11 ~= 90.9 Mb/s ~= 91 Mb/s
```

```
S2 = 909
```

__R2 - D :__
```
S1 = (91 / 91 + 909 + 100 + 100) * 800
= (91 / 1200) * 800
~= 60.66 Mb/s =~ 61Mb/s
```

```
S2 = (909 / 1200) * 800
~= 606 Mb/s
```

```
S3 = S4 = (100 / 1200) * 800
~= 66.66 Mb/s ~= 67 Mb/s
```

#### 2 : Fair Queueing

__R1 - R2 :__

```
F1 = 100 Mb/s
F2 = 900 Mb/s
```

__R2 - D :__

```
F1 = F3 = F4 = 100 Mb/s
F2 = 500 Mb/s
```

#### 3 : Max Min

|   F1    |   F2    |   F3    |   F3    |
|:-------:|:-------:|:-------:|:-------:|
| __100__ |   100   | __100__ | __100__ |
|         | __500__ |         |         |

# Fiche de révision pour le réseau sans fils

## Glosaire
RR : Radio Ressource
RRC: Radio Ressource Control
PDCP: Packet Data Convergence Protocol
MAC: Medium Access Control
P-GW = PDN-GW = Packet Data network Gateway; S-GW = Serving GW
HSS = Home Subscriber Server
MME: Mobility Management Entity
PCRF: Policy and Charging Rules Function (The PCRF in the visited network can contact the Home PCRF in case of roaming) (No RNC as in UMTS)

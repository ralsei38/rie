# Exam 2023

!!! note
    Examen de réseau sans fils de 2023

!!! warning
    Les sujet change légérement d'une années à l'autre.

## Sujet

[Sujet 2023 PDF](Sujet-2023.pdf)

## Correction
### 1 QCM

1.1) Dans le réseau GSM, la couche Radio Resource (RR) gère :

-[ ] Les fonctions de mobilité de l’abonné
-[x] Les canaux radio, y compris l’allocation des canaux dédiés
-[ ] La gestion de la localisation
-[x] La connexion de signalisation

1.2) Durant la communication entre la station de base et l’utilisateur, les informations sur la cellule sont envoyées via :

[ ] Stand Alone Dedicated Channel
[ ] Fast Associated Control Channel
[ ] Synchronisation Channel
[x] Broadcast Control Channel

1.3) Pour la trame UMTS-FDD la distance de duplex est :

[x] 190 MHz
[x] 45 MHz
[ ] 200 kHz
[ ] 175 kHz

1.4) Dans un système OFDM basé sur l’FFT classique, on utilise le préfix cyclique pour :

[ ] Contrôler la congestion du canal
[x] Remédier aux problèmes d’interférences entre symboles
[ ] Synchroniser les trames
[ ] Remédier aux problèmes d’interférences entre canaux

1.5) Un bloc de ressource a une largeur de 180 kHz composé de sous-porteuse d’une durée de 0,66μs. En justifiant votre réponse, la largeur de cette sous-porteuse est de :

[ ] 10 kHz
[ ] 15 kHz
[ ] 20 kHz
[ ] 25 kHz

F = 1/T = 1/0,66 = 1.5

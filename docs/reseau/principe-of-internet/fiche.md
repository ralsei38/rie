# Fiche de révison réseau

## Fiche revision TP

[fiche revision tp PDF](fiche-tp.pdf)


## Routage
__split-horizon :__ méchanisme de RIP qui empêche un routeur d'annoncé une route sur une interface depuis laquelle il à appris cette route

__Cas d'un réseau down :__ Dans le cadre de RIP, si un réseau venais à tomber, alors les routeurs directement connecté vont envoyer une route vers ce réseau avec le cout de *infini*. Puis le réseau sera supprimé au bout d'un certain temps

## TCP
__ACK Normal :__ Ack pour chaque paquet
__ACK cumulatif :__ Ack pour plusieurs paquets
__Fast retransmit :__ Si Ack duplique récu, alors il ya un paquet manquant, donc on renvoit le paquet en question

**Méchanisme TCP:**
- Selectvie repcat = si paquet perdu, alors on renvoit le paquet en question
- Go Back N = si paquet perdu, alors on revient au paquet n et envoie tout
- Step and Wait = Envoie d'un paquet puis attente de l'ACK pour renvoyer un autre paquets

__Cas d'un ACK cumulatif :__
Dans le cas d'ACK cumulatif, le client à une fenêtre (Win en W) égal aux nombre d'octets qu'il peut envoyer sans attendre d'ack
Début TCP = WIN/RTT

### Data gramme

```
TCP :                                             UDP :
+------------------------+----------------+       +-------------+-----------+
|       Source Port      |    Dest Port   |       | Source Port | Dest Port |
+------------------------+----------------+       +-------------+-----------+
|             Sequence number             |       |    Length   |  Checksum |
+-----------------------------------------+       +-------------+-----------+
|          Acknoladgement number          |       |           Data          |
+-------------+----------+------+---------+       +-------------+-----------+
| Data offset | Reserved | Flag | Windows |       
+------------------------+----------------+
|        Checksum        | Urgent pointer |
+------------------------+------+---------+
|            Options            | Padding |
+-------------------------------+---------+
|                   Data                  |
+-----------------------------------------+
```


## DNS

A : Association d'un nom à une ipv4 (ex zicolocloud.ddns.net -> 79.85.55.114)
CNAME : Association d'un nom à un nom (ex mc.zicolocloud.ddns.net -> zicolocloud.ddns.net)
PTR : Reverse A, donne un nom pour une adresse IP
MX : Définie le nom de serveur de mail du domaine
NS : Définie le serveur d'un sous-domaine
SOA : Start Of Autority, défini le serveur maitre du domaine
AXFR : Transfert de Zone

13 Server Root (de A à M)

# Exam 2018 - Réseaux IP

!!! note
    Examen de reseau de 2018

!!! warning
    Les sujet change de 2018 mais cela reste un bonne entrainement.

## Sujet

[Sujet 2018 PDF](exam-18.pdf)

## Note à savoir

### IPv6
ff02::1 Multicast pour les hote
ff02::2 Multicast pour les routeur


## 1 VLAN

| No | eth-src | eth-dst | type |     IP src     |     IP dst     | Protocol |    Details   |
|:--:|:-------:|:-------:|:----:|:--------------:|:--------------:|:--------:|:------------:|
|  1 |  22:11  |  ff:ff  |  ARP |        -       |        -       |     -    |   ARP Query  |
|  2 |  33:11  |  22:11  |  ARP |        -       |        -       |     -    | ARP Response |
|  3 |  22:11  |  33:11  | Ping |   129.88.42.1  |   129.88.48.2  |   ICMP   | echo request |
|  4 |  33:11  |  22:11  | Ping |   129.88.48.2  |   129.88.42.1  |   ICMP   |  echo reply  |
|  5 |  22:11  |  ff:ff  |  ARP |        -       |        -       |     -    |   ARP Query  |
|  6 |  55:11  |  22:11  |  ARP |        -       |        -       |     -    | ARP Response |
|  7 |  22:11  |  55:11  | Ping |   129.88.42.1  |  129.88.48.254 |   ICMP   | echo request |
|  8 |  66:11  |  ff:ff  |  ARP |        -       |        -       |     -    |   ARP Query  |
|  9 |  44:11  |  66:11  |  ARP |        -       |        -       |     -    | ARP Response |
| 10 |  66:11  |  44:11  | Ping | 200.195.128.25 |  200.195.128.1 |   ICMP   | echo request |
| 11 |  44:11  |  66:11  | Ping |  200.195.128.1 | 200.195.128.25 |   ICMP   |  echo reply  |
| 12 |  55:11  |  22:11  | Ping |  129.88.48.254 |   129.88.42.1  |   ICMP   |  echo reply  |

## 2 Adressage IPv6

2001:DB8:: /48
2001:DB8:0001:: /48
2001:DB8:0002:: /48
2001:DB8:0003:: /48

ping entre 2001:db8::1:20a:f7ff:fe03:d01c et 2001:db8::1:20a:f7ff:fe03:ce64

trames 6-14

6:
Salut, es ce que ya un gars qui s'appel éric ?
L'hote vérifie son adresse link local n'est pas déjà utiliser sur le réseau

12:
Ok moi j'mappel Éric alors, c'est quoi le nom de vôte group
L'hote demande au routeur le préfix global pour aller sur l'internet (vous verrez c'est facile)

13:
Yo Éric notre groupe c'est Eluveitie
Le routeur répond à l'hote

14:
Yo le réseau, moi c'est Éric de Eluveitie
L'hote vérifie si son adresse IPv6 Global est déjà utilisé sur le réseau

trames 18-19

Hé comment tu tappel?
djo !
Nique ta mère djo !

Récupération de l'adresse Mac de la cible

## Rootage
4)

| Préfixd de destination |    Next hop   | Interface | Distance |
|:----------------------:|:-------------:|:---------:|:--------:|
|          1.1.1         |    On Link    |   Eth 0   |     1    |
|          1.1.2         | R3 2.0.5.3/24 |   Eth 2   |     3    |
|          1.1.3         | R3 2.0.5.3/24 |   Eth 2   |     2    |
|          1.1.4         | R4 2.0.2.4/24 |   Eth 3   |     2    |
|          2.0.1         |    On Link    |   Eth 1   |     4    |
|          2.0.2         |    On Link    |   Eth 3   |     1    |
|          2.0.5         |    On link    |   Eth 2   |     1    |

5) R3 et R2 vont annoncer une distance de l'infinie entre eux au bout de 180 seconde puis l'information va se trensmettre aux autre routeurs

6) Le split horizon ne permet pas d'annoncer une distance au routeur qui lui à lui même appris la distance.En bref, les route sont à sens unique

## TCP
![shema TCP](./TCP.png)
